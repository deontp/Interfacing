.include "m16def.inc"

.def TMP1=R16
.def TMP2=R17
.def TMP3=R18
.def RXPOS=r19
.def ZERO=r2

.dseg
	RXBUFFER: .byte 10

.cseg
.org $000
    rjmp START
.org URXCaddr
    rjmp URXC_ISR
.org UTXCaddr
    rjmp UTXC_ISR

START:
    LDI TMP1, LOW(RAMEND)	;initialise the stack pointer
	OUT SPL, TMP1
	LDI TMP1, HIGH(RAMEND)
	OUT SPH, TMP1
    CALL INIT_UART
    CALL SEND_MENU
    LDI RXPOS, 0
	LDI TMP1, 0b00000000
	MOV ZERO, TMP1
	SEI
MAIN_LOOP:
	NOP
	NOP
	RJMP MAIN_LOOP

SEND_MENU:
    LDI ZL, low(2*TEXT_MENU)
    LDI ZH, high(2*TEXT_MENU)
	LPM TMP1, Z+
	OUT UDR, TMP1
    RET

INIT_UART:
    ;set baud rate (9600,8,n,2)
 	LDI TMP1, 51
	LDI TMP2, 0x00
	OUT UBRRH, TMP2
	OUT	UBRRL, TMP1
    ;set rx and tx enable
	SBI UCSRB, RXEN
	SBI UCSRB, TXEN
    ; enable uart interrupts
	SBI UCSRB, RXCIE
    SBI UCSRB, TXCIE
	RET

    ; Interrupt code for when UDR empty
    ; Code for TX complete
UTXC_ISR:
    LPM TMP1, Z+
    CPI TMP1, 0x00
    BREQ EXIT_ISR
    OUT UDR, TMP1


EXIT_ISR:
	RETI

; Code for RX complete
URXC_ISR:
	IN TMP1, UDR
	LDI XL,LOW(RXBUFFER)
    LDI ZH,HIGH(RXBUFFER)
    add XL, RXPOS
    adc xh, ZERO
    ST X,TMP1
    CPI TMP1, ')'
    BREQ PARSE_MESSAGE
    RETI

PARSE_MESSAGE:
    LDI XL,LOW(RXBUFFER)
    LDI XH,HIGH(RXBUFFER)
	RETI

; stored in program memory (CSEG)
TEXT_MENU:
    .db 0x1b,"[2J", 0x1b, "[H", "PROJECT TASKS: ",0x0d,0x0a, \
      "---------------", 0x0d, 0x0a, \
      "1) Single step clockwise", 0x0d,0x0a, \
      "2) Single step anti-clockwise", 0x0d,0x0a, \
      "3) 180 clockwise", 0x0d,0x0a, \
      "4) 180 anti-clockwise", 0x0d,0x0a, \
      "5) Disable Stepper", 0x0d,0x0a, \
      "6) Enable Stepper", 0x0d,0x0a, \
      "7) Start ADC PWM Task", 0x0d,0x0a, \
      "8) Stop ADC PWM Task", 0x0d,0x0a, \
      "9) Print 3rd stored message to LCD", 0x0d,0x0a, \
      "10) Clear LCD",0x0d,0x0a, \
      "11) Reset Micro-controller",0x0d,0x0a,0x00
