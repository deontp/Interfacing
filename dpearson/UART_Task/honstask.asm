; USART
; Connect RX and TX from usb-serial converter to TX and RX on board
; PB0 to led0
.include "m16def.inc"	

.def TMP1=R16		;
.def TMP2=R17		;
.def hundreds=r18
.def rxpos=r19
.def seconds=r20
.def fracseconds=r21
.def tens=r22
.def units=r23
.def stepsleft=r24
.def stepmask=r25
.def zero=r2
.dseg 
RXBUFFER: .byte 10 ; reserve 10bytes for the message
LCDbuffer: .byte 10 ; lcd time buffer
.cseg			
.org $000		;locate code at address $000
jmp START		; Jump to the START Label
.org URXCaddr
jmp URXC_ISR	
.org UTXCaddr
jmp UTXC_ISR
.org oc0addr
jmp oc0isr
.org OC2addr
jmp OC2_ISR

.org $02A		;locate code past the interupt vectors

START: 	
	ldi TMP1, LOW(RAMEND)	;initialise the stack pointer
	out SPL, TMP1
	ldi TMP1, HIGH(RAMEND)
	out SPH, TMP1
	call Init_LCD
	call Init_UART
    call Send_menu
	call initstepper
    ldi rxpos, 0
	ldi tmp1, 0x00
	mov zero, tmp1
	SEI
MAIN_LOOP:
	NOP
	NOP
	RJMP MAIN_LOOP

initstepper:
    ldi stepmask, 0x80
	ldi tmp1, 0xf0
	out ddrd, tmp1
    out portd, stepmask
	ret

OC2_ISR:
sbi ddrb,3
in tmp1, portb
ldi tmp2, 0b00001000
eor tmp1, tmp2
out portb, tmp1
   ;stop timer
   out tccr2, zero
   ;reset count
   out tcnt2, zero
   cpi stepsleft, 0 ; if no steps left, don't restart timer
   breq exitoc2isr
   ;restart timer
   ldi tmp1, 0b00000111
   out tccr2,tmp1
   call nextstep
   dec stepsleft
exitoc2isr:
   reti

nextstep:
   lsl stepmask
   cpi stepmask, 0x00
   brne dostepout 
   ldi stepmask, 0x10
dostepout:
   out PORTD, stepmask
   ret

oc0isr:
    inc fracseconds
	cpi fracseconds, 30
	breq incseconds
	reti
incseconds:
    inc seconds
    mov fracseconds, zero
	;update display
	call print_time_to_buffer
	ldi yl, low(LCDbuffer) 
    ldi yh, high(LCDbuffer) 
    ldi r16, 0x80
	call  Write_instruc
    ld r16, y+
    call  Write_char
    ld r16, y+
    call  Write_char
    ld r16, y+
    call  Write_char

;sbi ddrb,3
;in tmp1, portb
;ldi tmp2, 0b00001000
;eor tmp1, tmp2
;out portb, tmp1
	reti

send_menu:
    ; z is reserved for pointing to next char
    ldi zl, low(2*TEXT_MENU)
    ldi zh, high(2*TEXT_MENU)
	lpm tmp1, z+
	out UDR, tmp1
    ret

Init_UART:
;set baud rate (9600,8,n,2)
 	ldi Tmp1, 51
	ldi Tmp2, 0x00
	out UBRRH, Tmp2
	out	UBRRL, Tmp1
;set rx and tx enable
	sbi UCSRB, RXEN
	sbi UCSRB, TXEN
; enable uart interrupts
	sbi UCSRB, RXCIE
    sbi UCSRB, TXCIE
	RET

; Interrupt code for when UDR empty
; Code for TX complete 
UTXC_ISR:
 LPM tmp1, z+
 cpi tmp1, 0x00
 BREQ exittxisr
 out udr, tmp1
 reti

exittxisr:
;diable txcomplete int
  in tmp1, ucsrb
  andi tmp1, 0b10111111
  out ucsrb, tmp1  
	reti
; Code for RX complete
URXC_ISR:
    in tmp1, udr
    ldi xl,low(RXBUFFER)
    ldi xh,high(RXBUFFER)
	add xl, rxpos
	adc xh, zero
    st x,tmp1
    cpi tmp1, ')'
	breq parsemessage
	inc rxpos
	RETI

parsemessage:
     ldi xl,low(RXBUFFER)
    ldi xh,high(RXBUFFER)
	ld tmp1, x
	cpi tmp1, '1'
	breq Task1
	cpi tmp1, '2'
	breq Task2
	cpi tmp1, '3'
	breq Task3
exitparse:
    mov rxpos, zero
	reti

Task1:
    ; init t0 to oc in 20ms
    ldi tmp1, 160
	out ocr0, tmp1
	out tcnt0, zero
	in tmp1, timsk
	ori tmp1, 0x02
    out timsk, tmp1
	ldi tmp1, 0b00000101
	out tccr0, tmp1
	ldi r16, 0x01
    call Write_instruc
    mov seconds, zero
	call print_time_to_buffer
    jmp exitparse   

Task2:
    adiw xl,1
	ld tmp1, x+
	cpi tmp1,0x20
	breq getsteps
    jmp exitparse   
getsteps:

    ld tmp1, x
	cpi tmp1, 0x30
    brpl ok1
	jmp exitparse
ok1:
   cpi tmp1, 0x3a
    brlo ok2
	jmp exitparse
ok2:
   sbi ddrb,3
sbi portb,3
   andi tmp1, 0b00001111
   mov stepsleft, tmp1
   ; timer2 for steps
   ldi tmp1,0xf0 ;initial arb value for ocr2
   out ocr2, tmp1
   in tmp1, timsk ; enable oc2 interrupt in timsk
   ori tmp1, 0x80
   out timsk, tmp1
   ldi tmp1, 0b00000111 ; start t2 going as slowly as possible
   out tccr2, tmp1
   jmp exitparse
Task3:
    ldi tmp1, 0b00001000
	out wdtcr, tmp1
wdtreset:
    nop
    jmp wdtreset   

print_time_to_buffer:
   push seconds
    mov hundreds, zero
    mov tens, zero
    mov units, zero
findh:
    cpi seconds, 100
	brlo findt
    inc hundreds
	subi seconds, 100
	rjmp findh
findt:
    cpi seconds, 10
	brlo findu
   inc tens
	subi seconds, 10
   rjmp findt
findu:
    mov units, seconds
	ldi xl, low(LCDbuffer) 
	ldi xh, high(LCDbuffer) 
    ori hundreds, 0x30
    ori tens, 0x30
    ori units, 0x30
	st x+,hundreds
	st x+, tens
	st x+, units
	pop seconds
ret


TEXT_MENU: 
.db "1) start Stopwatch ",0x0d,0x0a, \
    "2) step stepper motor n steps", 0x0d, 0x0a,\
	"3) Watchdog reset.",0x0d,0x0a,0x00,0x00

.include "LCD.asm"
 
