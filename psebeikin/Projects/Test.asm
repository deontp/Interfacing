.include "m16def.inc"

.def TMP1=R16
.def TMP2=R17
.def TMP3=R18

.cseg

.org $000
rjmp START

.org $02A

START:	ldi TMP1, LOW(RAMEND)
		out SPL, TMP1
		ldi TMP1, HIGH(RAMEND)
		out SPH, TMP1

		sbi DDRB, 0
FLASH:	cbi PORTB, 0
		rcall Delay
		sbi PORTB, 0
		rcall Delay
		rjmp FLASH

Delay:	ser TMP1
Del1:	ser TMP2
Del2:	ldi TMP3, 20
Del3:	dec TMP3
				brne Del3
				dec TMP2
				brne Del2
				dec TMP1
				brne Del1
				ret
				.exit
