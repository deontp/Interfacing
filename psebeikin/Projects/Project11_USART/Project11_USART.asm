; USART Project
; Connect RX and TX from USB-Serial Converter to TX and RX on board

.include "m16def.inc"

.def TMP1 = R16
.def TMP2 = R18
.def TMP3 = R19
.def INDEX = R20
.def ZERO = R21

.eseg
.org 0x000
EE_LOOKUP: .db 0x30,"zero",0x00, \
			   0x31,"one",0x00, \
			   0x32,"two",0x00, \
			   0x33,"three",0x00, \
			   0x34,"four",0x00, \
			   0x35,"five",0x00, \
			   0x36,"six",0x00, \
	   		   0x37,"seven",0x00, \
			   0x38,"eight",0x00, \
			   0x39,"nine",0x00, 0x1b

.dseg
RAM_LOOKUP: .BYTE 20							; reserve 10 bytes for the message in SRAM

.cseg
.org $000									
	RJMP START
.org URXCaddr								; setup interrupt vectors
	RJMP URXC_ISR
.org UDREaddr
	RJMP UDRE_ISR
.org UTXCaddr
	RJMP UTXC_ISR

.org $02a									; locate code past the interrupt vectors

START:
	LDI TMP1, LOW(RAMEND)					; initialize stack pointer
	OUT SPL, TMP1
	LDI TMP1, HIGH(RAMEND)
	OUT SPH, TMP1	
	CALL STORE_IN_RAM
	CALL INIT_UART
	CLR ZERO
;	CALL SEND_MESSAGE	
	SEI

MAIN_LOOP:
	NOP
	NOP
	RJMP MAIN_LOOP

STORE_IN_RAM:
	LDI XL, LOW(EE_LOOKUP)
	LDI XH,	HIGH(EE_LOOKUP)
	LDI YL, LOW(RAM_LOOKUP)
	LDI YH,	HIGH(RAM_LOOKUP)

READ_BYTE:
	SBIC EECR, EEWE											; wait to make sure there is no active write
	RJMP READ_BYTE											; write still in progress so loop back to start
	OUT EEARH, XH											; record EEPROM address where the next read is going to come from
	OUT EEARL, XL
	SBI EECR, EERE											; enable read operation from EEPROM
	IN TMP2, EEDR											; store data i.e. 0x10		
	ST Y+, TMP2												; store in RAM and post-increment so the next piece of data can be stored in the next free location in RAM
	ADIW XL, 1												; moves to the next word that is stored in the EEPROM		
	CPI TMP2, 0x1b
	BREQ EXIT_READ
	RJMP READ_BYTE
EXIT_READ:
	RET

INIT_UART:
	LDI TMP1, 51							; set baud rate (9600, 8, n, 2)
	LDI TMP2, 0x00
	OUT UBRRH, TMP2
	OUT UBRRL, TMP1
	SBI UCSRB, RXEN
	SBI UCSRB, RXCIE
	RET

UDRE_ISR:									; what happens when UDR is empty
	RETI

;SEND_MESSAGE:
;	CBI UCSRB, RXEN
;	CBI UCSRB, RXCIE
;	SBI UCSRB, TXEN
;	SBI UCSRB, TXCIE						; enable UART transfer complete interrupt			
;	LDI ZL, LOW(RAM_LOOKUP)
;	LDI ZH, HIGH(RAM_LOOKUP)				; retrieve message address from SRAM		
;	LD TMP1, Z+
;	OUT UDR, TMP1
;	RET

URXC_ISR:		
	IN TMP1, UDR
	LDI INDEX, -1
FIND_NUM:
	INC INDEX
	LDI ZL, LOW(RAM_LOOKUP)
	LDI ZH, HIGH(RAM_LOOKUP)				; retrieve message address from SRAM		
	ADD ZL, INDEX
	ADC ZH, ZERO
	LD TMP2, Z
	CP TMP2, TMP1
	BRNE FIND_NUM
	LDI TMP1, 1
	ADD ZL, TMP1
	ADC ZH, ZERO
	LD TMP1, Z+
	SBI UCSRB, TXEN
	SBI UCSRB, TXCIE						; enable UART transfer complete interrupt
	OUT UDR, TMP1
;	CALL SEND_MESSAGE
	RETI

UTXC_ISR:									; what happens when a transmission completes	
	LD TMP1, Z+
	CPI TMP1, 0x00
	BREQ MESSAGE_FINISHED
	OUT UDR, TMP1
	RETI

MESSAGE_FINISHED:
	CBI UCSRB, TXEN
	CBI UCSRB, TXCIE						; disable receipt mode for next message	
	SBI UCSRB, RXEN
	SBI UCSRB, RXCIE	
	RETI
