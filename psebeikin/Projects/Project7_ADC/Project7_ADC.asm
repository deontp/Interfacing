; Portc0-7 connected to leds0-7
; ADC0 - VAR

.include "m16def.inc"					;Can you think why we need this

.def TMP1=R16							;defines serve the same purpose as in C,	
.def TMP2=R17							;before assembly, defined values are substituted
.def TMP3=R18

.cseg									;Tell the assembler that everything below this is in the code segment

.org $000								;locate code at address $000
	RJMP START							; Jump to the START Label
.org ADCCaddr
	RJMP ADC_ISR
.org $02A								;locate code past the interupt vectors

START: 
	LDI TMP1, LOW(RAMEND)				;initialise the stack pointer
	OUT SPL, TMP1
	LDI TMP1, HIGH(RAMEND)
	OUT SPH, TMP1

;setup ADC in free running mode, with interrupt
	LDI TMP1, 0b01100000				
	OUT ADMUX, TMP1						; REFS1 and REFS0 set to AVCC (only setting compatible with ATMega16)
	LDI TMP1, 0xff	
	OUT ADCSRA, TMP1					; enable all bits in ADC status and control register

	LDI TMP1, 0xff
	OUT PORTC, TMP1						; set PORTC initial state to 1
	OUT DDRC, TMP1						; set PORTC data direction mode to output
		
	SEI									; enable global interrupts

main_loop:
	NOP
	NOP
	RJMP main_loop

ADC_ISR:
	PUSH TMP1
	IN TMP1, ADCH
	OUT PORTC, TMP1
	POP TMP1
	RETI
