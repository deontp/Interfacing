; Portc0-7 connected to leds0-7
; ADC0 - VAR

.include "m16def.inc"					;Can you think why we need this

.def TMP1=R16							;defines serve the same purpose as in C,	
.def TMP2=R17							;before assembly, defined values are substituted
.def TMP3=R18

.cseg									;Tell the assembler that everything below this is in the code segment

.org $000								;locate code at address $000
	RJMP START							; Jump to the START Label
.org ADCCaddr
	RJMP ADC_ISR
.org $02A								;locate code past the interupt vectors

START: 
	LDI TMP1, LOW(RAMEND)				;initialise the stack pointer
	OUT SPL, TMP1
	LDI TMP1, HIGH(RAMEND)
	OUT SPH, TMP1

;setup ADC in free running mode, with interrupt
	LDI TMP1, 0b01100000				
	OUT ADMUX, TMP1						; REFS1 and REFS0 set to AVCC (only setting compatible with ATMega16)
	LDI TMP1, 0xff	
	OUT ADCSRA, TMP1					; enable all bits in ADC status and control register

	LDI TMP1, 0xff
	OUT PORTC, TMP1						; set PORTC initial state to 1
	OUT DDRC, TMP1						; set PORTC data direction mode to output
		
	SEI									; enable global interrupts

main_loop:
	NOP
	NOP
	RJMP main_loop

ADC_ISR:
	PUSH TMP1
	
	IN TMP1, ADCH
	LDI TMP2, 0b11111111

LED8:	CP TMP1, TMP2
		BRLO LED7
		OUT PORTC, TMP2
		RJMP EXIT
LED7:	ANDI TMP2, 0b01111111
		CP TMP1, TMP2
		BRLO LED6
		OUT PORTC, TMP2
		RJMP EXIT
LED6:	ANDI TMP2, 0b00111111
		CP TMP1, TMP2
		BRLO LED5
		OUT PORTC, TMP2
		RJMP EXIT
LED5:	ANDI TMP2, 0b00011111
		CP TMP1, TMP2
		BRLO LED4
		OUT PORTC, TMP2
		RJMP EXIT
LED4:	ANDI TMP2, 0b00001111
		CP TMP1, TMP2
		BRLO LED3
		OUT PORTC, TMP2
		RJMP EXIT
LED3:	ANDI TMP2, 0b00000111
		CP TMP1, TMP2
		BRLO LED2
		OUT PORTC, TMP2
		RJMP EXIT
LED2:	ANDI TMP2, 0b00000011
		CP TMP1, TMP2
		BRLO LED1
		OUT PORTC, TMP2
		RJMP EXIT
LED1:	ANDI TMP2, 0b00000001
		CP TMP1, TMP2
		BRLO LED0
		OUT PORTC, TMP2
		RJMP EXIT
LED0:	CLR TMP2
		OUT PORTC, TMP2

EXIT:	POP TMP1
		RETI
