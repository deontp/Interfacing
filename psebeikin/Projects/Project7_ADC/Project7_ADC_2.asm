; Portc0-7 connected to leds0-7
; ADC0 - VAR

.include "m16def.inc"					;Can you think why we need this

.def TMP1=R16							;defines serve the same purpose as in C,	
.def TMP2=R17							;before assembly, defined values are substituted
.def TMP3=R18

.cseg									;Tell the assembler that everything below this is in the code segment

.org $000								;locate code at address $000
	RJMP START							; Jump to the START Label
.org INT0addr
	RJMP INT0_ISR
.org $02A								;locate code past the interupt vectors

START: 
	LDI TMP1, LOW(RAMEND)				;initialise the stack pointer
	OUT SPL, TMP1
	LDI TMP1, HIGH(RAMEND)
	OUT SPH, TMP1

	LDI TMP1, 0b01100000				
	OUT ADMUX, TMP1						; REFS1 and REFS0 set to AVCC (only setting compatible with ATMega16)
	LDI TMP1, 0b10000111				; setup ADC in single conversion mode
	OUT ADCSRA, TMP1
	
	CLR TMP1
	OUT PORTC, TMP1						; set PORTC initial state to 0
	OUT DDRD, TMP1						; set PORTB data direction to input
	
	LDI TMP1, 0xff	
	OUT DDRC, TMP1						; set PORTC data direction mode to output
	OUT PORTD, TMP1						; enable pull-ups on PORTB

	LDI TMP1, 0x03						; interrupt triggers on falling edge of INT0
	OUT MCUCR, TMP1
	
	LDI TMP1, 0x40						; enable interrupt INT0
	OUT GICR, TMP1
		
	SEI									; enable global interrupts

main_loop:
	NOP
	NOP
	RJMP main_loop

INT0_ISR:
	PUSH TMP1							; conserve TMP1 register	

	IN TMP1, GIFR						; manually disable flag to prevent switch bounce
	ANDI TMP1, 0b01000000	
	OUT GIFR, TMP1

	IN TMP1, SREG						; transfer status register into TMP1
	PUSH TMP1							; save sreg

	LDI TMP1, 0x40
	IN TMP2, ADCSRA
	OR TMP2, TMP1						; enable ADSC bit in ADCSRA to start conversion
	OUT ADCSRA, TMP2

	RCALL DELAY

	IN TMP1, ADCH
	OUT PORTC, TMP1						; display output on LEDs

	POP TMP1							; pop the status register off of the stack and store in TMP1
	OUT SREG, TMP1						; restore sreg	
	
	POP TMP1							; restore tmp1

	RETI

DELAY:
	PUSH TMP3
	PUSH TMP2
	PUSH TMP1

		SER TMP1		; TMP1=0xff
DEL1:	SER TMP2		; TMP2=0xff
DEL2:	LDI TMP3, 5		; TMP=10 decimal
DEL3:	DEC TMP3		; decrement TMP3
		BRNE DEL3
		DEC TMP2
		BRNE DEL2
		DEC TMP1
		BRNE DEL1
	POP TMP1
	POP TMP2
	POP TMP3
		RET
