; Stepper Motor 1
; PD2 - SW0
; PD3 - SW1
; PD4 - DRV0
; ..
; PD7 - DRV3
; Also make sure that the jumper by the motor 
; Connection is set to 2-3 (+5v)


.include "m16def.inc"	

.def TMP1=R16		;
.def TMP2=R17		;
.def TMP3=R18
.def mask=R19
.def dir=R20
.def COUNT=R21
.cseg			
.org $000		;locate code at address $000
rjmp START		; Jump to the START Label
.org INT0addr
rjmp INT0_ISR
.org INT1addr
rjmp INT1_ISR
.org OVF0addr
rjmp T0_OVF_ISR
.org OC0addr
rjmp OC0_ISR
.org $02A		;locate code past the interupt vectors

START: 	
	LDI TMP1, LOW(RAMEND)	;initialise the stack pointer
	OUT SPL, TMP1
	LDI TMP1, HIGH(RAMEND)
	OUT SPH, TMP1

	RCALL SETUP_PORTS
	RCALL SETUP_INTERRUPTS
	RCALL SETUP_TIMER
	LDI dir, 0x01			; sets the stepper motor going clockwise
	
	SEI

MAIN_LOOP:
  NOP
  NOP
  NOP
  RJMP MAIN_LOOP

SETUP_PORTS:
	; Set PORT D to correct IO
	LDI TMP1, 0xff
	OUT DDRA, TMP1
	CLR TMP1
	OUT PORTA, TMP1
	LDI TMP1, 0xf0
	OUT DDRD, TMP1
	SBI PORTD, 2		; enable pull-ups on portD
	
	RET

SETUP_INTERRUPTS:
	; setup int0 and int1 interrupts
	LDI TMP1, 0x0a			; interrupts trigger on falling edge
	OUT MCUCR, TMP1	
	LDI TMP1, 0x40
	OUT GICR, TMP1
	RET

SETUP_TIMER:	
	LDI TMP1, 0x00
	OUT TCNT0, TMP1			; set timer value to zero
	LDI TMP1, 0x03
	OUT TIMSK, TMP1			; setup timer 0 for overflow	
	LDI MASK, 0b00010000
	IN TMP1, PORTD
	OR TMP1, MASK
	OUT PORTD, TMP1			; set the starting point for the stepper motor 
	LDI TMP1, 80
	OUT OCR0, TMP1 
	LDI TMP1, 0x05			; set pre-scalar to 1024 / start timer
	OUT TCCR0, TMP1	
	RET

DELAY:
		PUSH TMP3
		PUSH TMP2
		PUSH TMP1
		SER TMP1		; TMP1=0xff
Del1:	SER TMP2		; TMP2=0xff
Del2:	LDI TMP3, 20 	; TMP=10 decimal
Del3:	DEC TMP3		; decrement TMP3
		BRNE Del3
		DEC TMP2
		BRNE Del2
		DEC TMP1
		BRNE Del1
		POP TMP1
		POP TMP2
		POP TMP3
		RET

INT0_ISR:
	LDI TMP1, 0x00
	OUT TCCR0, TMP1		; stop timer
	OUT GICR, TMP1		; disable INT0
	;;RCALL Delay			; create delay	
	LDI TMP1, 0x40		
	OUT GIFR, TMP1		; clear flags for INT0, INT1

	LDI TMP1, 0x01
	EOR dir, TMP1	
	;LDI dir, 0x00
	SBRS dir, 0
	LDI MASK, 0b10000000
	SBRC dir, 0
	LDI MASK, 0b00010000

	LDI TMP1, 0x05
	OUT TCCR0, TMP1		; restart timer
	LDI TMP1, 0x40
	OUT GICR, TMP1		; enable INT0
	RETI

INT1_ISR:
/*	OUT TCCR0, TMP1		; stop timer
	OUT GICR, TMP1		; disable INT0, INT1
	RCALL Delay		
	LDI TMP1, 0xc0
	OUT GIFR, TMP1		; clear flags for INT0, INT1

	LDI MASK, 0b00010000
	LDI dir, 0x01

	LDI TMP1, 0x03
	OUT TCCR0, TMP1		; restart timer
	LDI TMP1, 0x40
	OUT GICR, TMP1*/		; enable INT0
	RETI

OC0_ISR:
	INC COUNT
	OUT PORTA, TMP1
	RETI
	

T0_OVF_ISR:
	; single step through pd4-7
	SBRS dir, 0
    LSR MASK
	SBRC dir, 0
	LSL MASK	    
	CPI MASK, 0b00001000
	BREQ SET_PD4_PD7   
	CPI MASK, 0x00
	BREQ SET_PD4_PD7	
	;inc mask     
    RJMP do_out

SET_PD4_PD7:
	SBRS dir, 0
    LDI MASK, 0b10000000
	SBRC dir, 0
	LDI MASK, 0b00010000

DO_OUT:
    IN TMP1, PORTD;
	ANDI TMP1, 0x0f
	OR TMP1, MASK
	OUT PORTD, TMP1
	RETI
