; Stepper Motor 1
; PD2 - SW0
; PD3 - SW1
; PD4 - DRV0
; ..
; PD7 - DRV3
; Also make sure that the jumper by the motor 
; Connection is set to 2-3 (+5v)


.include "m16def.inc"	

.def TMP1=R16		;
.def TMP2=R17		;
.def TMP3=R18
.def mask=R19
.cseg			
.org $000		;locate code at address $000
rjmp START		; Jump to the START Label
.org INT0addr
rjmp INT0_ISR
.org INT1addr
rjmp INT1_ISR
.org OC0addr
rjmp OC0_ISR
.org $02A		;locate code past the interupt vectors

START: 	
	LDI TMP1, LOW(RAMEND)	;initialise the stack pointer
	OUT SPL, TMP1
	LDI TMP1, HIGH(RAMEND)
	OUT SPH, TMP1

	RCALL SETUP_PORTS
	RCALL SETUP_INTERRUPTS
	RCALL SETUP_TIMER
	RCALL SETUP_MOTOR

	SEI

MAIN_LOOP:
  NOP
  NOP
  NOP
  RJMP MAIN_LOOP

SETUP_PORTS:		
	LDI TMP1, 0xf0		
	OUT DDRD, TMP1		; set PORTD to input mode
	SBI PORTD, 2		; enable pull-ups on bit 2 of PORTD
	SBI PORTD, 3		; enable pull-ups on bit 3 of PORTD	
	RET

SETUP_INTERRUPTS:
	; setup int0 and int1 interrupts
	LDI TMP1, 0x0a			; interrupts trigger on falling edge
	OUT MCUCR, TMP1	
	LDI TMP1, 0xc0			
	OUT GICR, TMP1			; enable int0 and int1 interrupts
	RET

SETUP_TIMER:	
	LDI TMP1, 0x00
	OUT TCNT0, TMP1			; set timer value to zero
	LDI TMP1, 0x02
	OUT TIMSK, TMP1			; setup timer 0 for output compare		
	LDI TMP1, 0b00001101	; set pre-scalar to 1024 / start timer
	OUT TCCR0, TMP1
	LDI TMP1, 0x80
	OUT OCR0, TMP1			; set output compare register to 128
	RET
	
SETUP_MOTOR:
	LDI MASK, 0b00010000
	IN TMP1, PORTD
	OR TMP1, MASK
	OUT PORTD, TMP1			; set the starting point for the stepper motor 		
	RET

INT0_ISR:				; slow down the motor
	;LDI TMP1, 0xc0		
	;OUT GIFR, TMP1		; clear flags for INT0, INT1
	;LDI TMP1, 0x00
	;OUT GICR, TMP1		; disable interrupts on INT0, INT1
	PUSH TMP1
	IN TMP1, SREG
	PUSH TMP1
	

	IN TMP2, OCR0
	LDI TMP1, 0x02
	ADD TMP2, TMP1		; increase output compare register - slow down motor
	OUT OCR0, TMP2	
	
	;LDI TMP1, 0xc0
	;OUT GICR, TMP1		; enable INT0 and INT1

	POP TMP1
	OUT SREG, TMP1
	POP TMP1
		
	RETI

INT1_ISR:				; speed up the motor
	;LDI TMP1, 0xc0		
	;OUT GIFR, TMP1		; clear flags for INT0, INT1
	;LDI TMP1, 0x00
	;OUT GICR, TMP1		; disable interrupts on INT0, INT1
	PUSH TMP1
	IN TMP1, SREG
	PUSH TMP1

		
	IN TMP2, OCR0
	LDI TMP1, 0x02
	SUB TMP2, TMP1		; decrease output compare register - speed up motor
	OUT OCR0, TMP2		

	;LDI TMP1, 0xc0
	;OUT GICR, TMP1		; enable INT0 and INT1
	POP TMP1
	OUT SREG, TMP1
	POP TMP1
		
	RETI

OC0_ISR:
	LSL MASK	
	BREQ SET_PD4_PD7
    RJMP do_out

SET_PD4_PD7:
	LDI MASK, 0b00010000

DO_OUT:
    IN TMP1, PORTD;
	ANDI TMP1, 0x0f
	OR TMP1, MASK
	OUT PORTD, TMP1
	;CLR TMP1	
	;OUT TCNT0, TMP1
	RETI

DELAY:
		PUSH TMP3
		PUSH TMP2
		PUSH TMP1
		SER TMP1		; TMP1=0xff
Del1:	SER TMP2		; TMP2=0xff
Del2:	LDI TMP3, 20 	; TMP=10 decimal
Del3:	DEC TMP3		; decrement TMP3
		BRNE Del3
		DEC TMP2
		BRNE Del2
		DEC TMP1
		BRNE Del1
		POP TMP1
		POP TMP2
		POP TMP3
		RET
