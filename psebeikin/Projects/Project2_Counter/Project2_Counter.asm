;button_count
; This program counts button presses on 
; portd0 and displays them on portb leds
; Don't forget to connect portb0-7 to leds 0-7
; and connect portd0 to switch 0
.include "m16def.inc"	;Can you think why we need this

.def TMP1=R16		;
.def TMP2=R17		;
.def TMP3=R18
.def COUNT=R19	;store the count in this register

.cseg			;Tell the assembler that everything below this is in the code segment

.org $000		;locate code at address $000
rjmp START		; Jump to the START Label

.org $02A		;locate code past the interupt vectors

START: 	
	ldi TMP1, LOW(RAMEND)	;initialise the stack pointer
	out SPL, TMP1
	ldi TMP1, HIGH(RAMEND)
	out SPH, TMP1

	RCALL INITIALISE

SAMPLE_DOWN:
	SBIC PIND,0  ; skip if button pushed
	RJMP SAMPLE_DOWN ;
	;If we get here the button has been pushed
	INC COUNT	;increment the counter;
	MOV TMP1, COUNT
	OUT PORTA, TMP1
SAMPLE_UP:
	SBIS PIND, 0 ; skip if button released
	RJMP SAMPLE_UP
	RJMP SAMPLE_DOWN

INITIALISE:
	; Setup port a as output for LEDs 
	; and inital state to all off and set counter to zero
	SER TMP1 			; set all bits in TMP1 
	OUT DDRA, TMP1		; set all ports to output because TMP1 was set.
    CLR TMP1			; clear all bits in TMP1
	OUT PORTA, TMP1 	; 0x00 = all off
	CLR COUNT			; clear COUNT	
	OUT DDRD, TMP1 		; make d inputs (tmp1 is a handy clear register)
    SER TMP1			; set all bits in TMP1
	OUT PORTD, TMP1 	; activate pullups on PORTD
	RET
