;button_count
; This program counts button presses on 
; portd0 and displays them on portb leds
; Don't forget to connect portb0-7 to leds 0-7
; and connect portd0 to switch 0
.include "m16def.inc"	;Can you think why we need this

.def TMP1=R16		;
.def TMP2=R17		;
.def TMP3=R18
.def COUNT=R19	;store the count in this register

.cseg			;Tell the assembler that everything below this is in the code segment

.org $000		;locate code at address $000
rjmp START		; Jump to the START Label

.org $02A		;locate code past the interupt vectors

START: 	
	ldi TMP1, LOW(RAMEND)	;initialise the stack pointer
	out SPL, TMP1
	ldi TMP1, HIGH(RAMEND)
	out SPH, TMP1

	RCALL INITIALISE

CHECK_BUTTON:
	SBIS PIND, 0		; skip if button is not pushed
	RCALL BUTTON_INC_DOWN
	SBIS PIND, 1		; skip if button is not pushed
	RCALL BUTTON_DEC_DOWN	
	RJMP CHECK_BUTTON

BUTTON_INC_DOWN:	
   ;If we get here the button has been pushed
	INC COUNT			; increment the counter;
	MOV TMP1, COUNT		; move value in count to a register
	OUT PORTA, TMP1		; output count value	

BUTTON_INC_UP:
	SBIC PIND, 0 		; skip if button released
	RET	; idle - go back to checking for button presses
	RJMP BUTTON_INC_UP	; button has not been released
	

BUTTON_DEC_DOWN:
;If we get here the button has been pushed
	DEC COUNT			;decrement the counter;
	MOV TMP1, COUNT		; move value in count to a register
	OUT PORTA, TMP1		; output count value

BUTTON_DEC_UP:
	SBIC PIND, 1 		; skip if button released
	RET	; idle - go back to checking for button presses
	RJMP BUTTON_DEC_UP	; button has not been released

INITIALISE:
	; Setup port a as output for LEDs 
	; and inital state to all off and set counter to zero
	SER TMP1 			; set all bits in TMP1 
	OUT DDRA, TMP1		; set all ports to output because TMP1 was set.
    CLR TMP1			; clear all bits in TMP1
	OUT PORTA, TMP1 	; 0x00 = all off
	CLR COUNT			; clear COUNT	
	OUT DDRD, TMP1 		; make d inputs (tmp1 is a handy clear register)
    SER TMP1			; set all bits in TMP1
	OUT PORTD, TMP1 	; activate pullups on PORTD
	RET
