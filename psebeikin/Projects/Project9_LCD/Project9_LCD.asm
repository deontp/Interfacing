; working with the LCD
; PORTC 0 - 7 connected to LCD_data 0 - 7
; PB0 - E
; PB1 - rw
; PB2 - rs

.include "m16def.inc"

.def TMP1 = R16
.def TMP2 = R17
.def TMP3 = R18

.cseg
.org $000
	RJMP START													; jump to start label
.org $02A														;locate code past the interrupt vectors

START:
	LDI TMP1, LOW(RAMEND)										; initialize stack pointer
	OUT SPL, TMP1
	LDI TMP1, HIGH(RAMEND)
	OUT SPH, TMP1

	CALL INIT_LCD	
	; hello
	LDI TMP1, 0x48
	CALL WRITE_CHAR
	LDI TMP1, 0x45
	CALL WRITE_CHAR
	LDI TMP1, 0x4C
	CALL WRITE_CHAR
	CALL WRITE_CHAR
	LDI TMP1, 0x4F
	CALL WRITE_CHAR
	LDI TMP1, 0x20
	CALL WRITE_CHAR
	
	;world
	LDI TMP1, 0x57
	CALL WRITE_CHAR
	LDI TMP1, 0x4F
	CALL WRITE_CHAR
	LDI TMP1, 0x52
	CALL WRITE_CHAR
	LDI TMP1, 0x4C
	CALL WRITE_CHAR
	LDI TMP1, 0x44
	CALL WRITE_CHAR

	;LDI TMP1, 0x84
	;CALL WRITE_INSTRUC
	;RCALL DELAY
	;LDI TMP1, 0x7e
	;CALL WRITE_CHAR
	;LDI TMP1, 0x55
	;CALL WRITE_CHAR

MAIN_LOOP:
	NOP
	NOP
	RJMP MAIN_LOOP

.include "LCD.asm"
