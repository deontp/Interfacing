; working with the LCD
; PORTC 0 - 7 connected to LCD_data 0 - 7
; PB0 - E
; PB1 - rw
; PB2 - rs

.include "m16def.inc"

.def TMP1 = R16
.def TMP2 = R17
.def TMP3 = R18
.def COUNT = R19
.def ZERO = R20
.def SPARE = R21

.eseg
.org 0x000
EE_LOOKUP: .db "Howzit boet?", 0x00

.dseg
RAM_LOOKUP: .BYTE 10

.cseg
.org $000
	RJMP START													; jump to start label
.org $02A														;locate code past the interrupt vectors

START:
	LDI TMP1, LOW(RAMEND)										; initialize stack pointer
	OUT SPL, TMP1
	LDI TMP1, HIGH(RAMEND)
	OUT SPH, TMP1

	CLR ZERO	
	RCALL STORE_IN_RAM
	RCALL DISPLAY_LCD

MAIN_LOOP:
	NOP
	NOP
	RJMP MAIN_LOOP


STORE_IN_RAM:
	LDI XL, LOW(EE_LOOKUP)
	LDI XH,	HIGH(EE_LOOKUP)
	LDI YL, LOW(RAM_LOOKUP)
	LDI YH,	HIGH(RAM_LOOKUP)

READ_BYTE:
	SBIC EECR, EEWE											; wait to make sure there is no active write
	RJMP READ_BYTE											; write still in progress so loop back to start
	OUT EEARH, XH											; record EEPROM address where the next read is going to come from
	OUT EEARL, XL
	SBI EECR, EERE											; enable read operation from EEPROM
	IN TMP2, EEDR											; store data i.e. 0x10		
	ST Y+, TMP2												; store in RAM and post-increment so the next piece of data can be stored in the next free location in RAM
	ADIW XL, 1												; moves to the next word that is stored in the EEPROM		
	CPI TMP2, 0x00
	BREQ EXIT_READ
	RJMP READ_BYTE
EXIT_READ:
	RET

DISPLAY_LCD:
	CALL INIT_LCD

READ_FROM_RAM:
	LDI XL, LOW(RAM_LOOKUP)
	LDI XH, HIGH(RAM_LOOKUP)
LOOP:	
	LD TMP1, X
	CPI TMP1, 0x00
	BREQ EXIT
	CALL WRITE_CHAR	
	LDI TMP1, 0x01
	ADD XL, TMP1
	ADC XH, ZERO
	RJMP LOOP
EXIT:
	RET

.include "LCD.asm"
	
	



