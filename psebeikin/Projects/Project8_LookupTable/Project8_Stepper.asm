; Stepper Motor 1
; PD2 - SW0
; PD3 - SW1
; PD4 - DRV0
; ..
; PD7 - DRV3
; Also make sure that the jumper by the motor 
; Connection is set to 2-3 (+5v)


.include "m16def.inc"	

.def TMP1=R16		;
.def TMP2=R17		;
.def TMP3=R18
.def mask=R19

.eseg
.org 0x000
EE_LOOKUP:	.db 0x00,0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80; the numbers 0 - 7 representing the LEDs	

.dseg
RAM_LOOKUP:	.BYTE 10 										; reserve 10 bytes of RAM for lookuptable

.cseg			
.org $000		;locate code at address $000
rjmp START		; Jump to the START Label

.org $02A		;locate code past the interupt vectors

START: 	
	ldi TMP1, LOW(RAMEND)	;initialise the stack pointer
	out SPL, TMP1
	ldi TMP1, HIGH(RAMEND)
	out SPH, TMP1
  
  LDI TMP1, 0xff
  OUT DDRA, TMP1
  OUT PORTA, TMP1

MAIN_LOOP:
	NOP
	NOP
	RJMP MAIN_LOOP

READ_LOOKUP_FROM_EEPROM:
;Set up address registers.
	LDI XL, LOW(EE_LOOKUP)
	LDI XH, HIGH(EE_LOOKUP)
	LDI YL, LOW(RAM_LOOKUP)
	LDI YH,	HIGH(RAM_LOOKUP)
    LDI TMP1, 0x00											; start at the first item in the lookup table
READ_BYTE:
	SBIC EECR, EEWE											; wait to make sure there is no active write
	RJMP READ_BYTE											; write still in progress so loop back to start
	OUT EEARH, XH											; record EEPROM address where the next read is going to come from
	OUT EEARL, XL
	SBI EECR, EERE											; enable read operation from EEPROM
	IN TMP2, EEDR											; store data i.e. 0x10	
	ST Y+, TMP2												; store in RAM and post-increment so the next piece of data can be stored in the next free location in RAM
	ADIW XL, 1												; moves to the next word that is stored in the EEPROM
	INC TMP1												; used to index into the lookup table, increment so can index into the next item on next iteration.
	CPI TMP1, 8												; have we gone through all 8 items in the lookup table?
	BRNE READ_BYTE											; if not, loop back to top and read in next item, else exit.
	RET
