.include "m16def.inc"	

.def ZERO=R19
.def TMP1=R16		
.def TMP2=R17		
.def TMP3=R18
.def DIR = R21												; determines which direction the motor is going in
.def NEXT = R20												; denotes the next step to be taken by the motor, loading in a value from RAM	
.def COUNT = R22

.eseg
.org 0x000
EE_LOOKUP: .db 0x80,0xc0,0x40,0x60,0x20,0x30,0x10, 0x00  	; half-steps for the stepper motor
;EE_LOOKUP: .db 0x80, 0x40, 0x20, 0x10, 0x00					; full-steps for the stepper motor

.dseg
RAM_LOOKUP:	.BYTE 10 										; reserve 10 bytes of RAM for lookuptable

.cseg 														; Tell the assembler that everything below this is in the code segment
;.org OVF0addr
	;RJMP OVF0_ISR
.org OC0addr
	RJMP OC0_ISR
;.org INT0addr
	;RJMP INT0_ISR
;.org INT1addr
	;RJMP INT1_ISR
.org 0x000													; locate code at address $000
RJMP START													; Jump to the START Label

.org $02A													; locate code past the interupt vectors

START: 	
	LDI TMP1, LOW(RAMEND)									; initialise the stack pointer
	OUT SPL, TMP1
	LDI TMP1, HIGH(RAMEND)
	OUT SPH, TMP1
	RCALL READ_LOOKUP_TABLE
	RCALL SETUP_PORTS
	;RCALL SETUP_INTERRUPTS	
	RCALL SETUP_TIMER	
	;RCALL SETUP_MOTOR	
	SEI														; enable global interrupts

MAIN_LOOP:
	NOP
	NOP
	NOP
	RJMP MAIN_LOOP

;SETUP_MOTOR:		
;	LDI DIR, 0x01											; motor going clockwise
;	LDI MASK, 0x10	
;	OUT PORTD, MASK
	RET

SETUP_PORTS:
	LDI TMP1, 0xf0											
	OUT DDRD, TMP1											; set PD4-PD7 in output mode; set PD0 in output mode for test/debug LED
	LDI TMP1, 0x00											; set initial values of stepper motor to zero and initialize pullups on PD2 and PD3
	OUT PORTD, TMP1											
	;LDI TMP1, 0x0a	
	;OUT MCUCR, TMP1											; set int0 and int1 interrupts to trigger on falling edge
	RET

;SETUP_INTERRUPTS:
											; enable timer0 output compare interrupt
	;LDI TMP1, 0xc0											
	;OUT GICR, TMP1											; enable interrupts for int0 and int1
	;RET

SETUP_TIMER:
	CLR TMP1
	OUT TCNT0, TMP1											; start timer off at zero
	LDI TMP1, 0x02											; enable output compare interrupt
	OUT TIMSK, TMP1		
	LDI TMP1, 0b00001010									; use a pre-scalar of 64 for timer0
	OUT TCCR0,  TMP1
	LDI TMP1, 0x80
	OUT OCR0, TMP1											; start timer
	RET

READ_LOOKUP_TABLE:
	LDI XL, LOW(EE_LOOKUP)									; read in EEPROM address
	LDI XH, HIGH(EE_LOOKUP)			
	LDI YL, LOW(RAM_LOOKUP)									; read in RAM address
	LDI YH, HIGH(RAM_LOOKUP)	
	CLR TMP1

READ_BYTE:
	SBIC EECR, EEWE											; check if there is a write going on
	RJMP READ_BYTE											; loop back to start if write is happening
	OUT EEARH, XH											; read in EEPROM address
	OUT EEARL, XL
	SBI EECR, EERE											; enable read operation on EEPROM
	IN TMP1, EEDR											; store value
	ST Y+, TMP1												; store in RAM, increment to next free position in RAM
	ADIW XL, 1
	CPI TMP1, 0x00											; have all the items in lookup table been read in?
	BRNE READ_BYTE
	RET

OC0_ISR:	
LOOP:
	LDI XL, LOW(RAM_LOOKUP)									; load address from RAM
	LDI XH, HIGH(RAM_LOOKUP)
	ADD XL, NEXT											; index into the correct position
	ADC XH, ZERO
	LD TMP2, X
	CPI TMP2, 0x00											; if we have processed all 8 eight steps, wrap around to first one
	BRNE STEP_MOTOR	
	MOV NEXT, ZERO
	RJMP LOOP	

STEP_MOTOR:
	IN TMP1, PORTD
	ANDI TMP1, 0x0f
	OR TMP1, TMP2
	OUT PORTD, TMP1	
	INC NEXT		
	RETI
	;CPI DIR, 0x01											; check for clockwise direction
	;BRNE ANTI_CLOCKWISE
;ANTI_CLOCKWISE:
;	DEC NEXT
;	CPI NEXT, -1
;	BRNE EXIT
;	LDI NEXT, 7
;	RETI


	;LSL MASK
	;BREQ SET_MOTOR	
	;RJMP OUTPUT
;SET_MOTOR:
;	LDI MASK, 0x10

;OUTPUT:
;	IN TMP1, PORTD;
;	ANDI TMP1, 0x0f
;	OR TMP1, MASK
;	OUT PORTD, MASK	
;	RETI

;OVF0_ISR:	
	;SBI PORTD, 0	
	
				
;	CPI DIR, 0x01											; check for clockwise direction
;	BRNE ANTI_CLOCKWISE
	;INC NEXT												; go to next index
	;CPI NEXT, 7												; if we have processed all 8 eight steps, wrap around to first one
	;BRNE EXIT
	;MOV NEXT, ZERO



;INT0_ISR:
	;SBI PORTD, 0
;	LDI TMP1, GIFR
;	ANDI TMP1, 0b01000000									; turn off interrupt flag for int0
;	OUT GIFR, TMP1	
;	LDI TMP1, GICR
;	ANDI TMP1, 0b10000000
;	OUT GICR, TMP1											; turn off interrupt for int0
;	RCALL DELAY
;	LDI TMP1, 0x01
;	EOR DIR, TMP1											; toggle direction	
;	LDI TMP1, GICR
;	ANDI TMP1, 0b11000000									; turn interrupt on for int1 and int0
;	OUT GICR, TMP1
;	RETI

;INT1_ISR:
	;SBI PORTD, 0
;	LDI TMP1, GIFR
;	ANDI TMP1, 0b10000000									; turn off interrupt flag for int1
;	OUT GIFR, TMP1	
;	LDI TMP1, GICR
;	ANDI TMP1, 0b01000000
;	OUT GICR, TMP1											; turn off interrupt for int1
;	RCALL DELAY
;	LDI TMP1, 0b00000011
;	IN TMP2, TCCR0
;	EOR TMP2, TMP1											; toggle timer on/off
;	OUT TCCR0, TMP2
;	LDI TMP1, GICR
;	ANDI TMP1, 0b11000000									; turn interrupt on for int1 and int0
;	OUT GICR, TMP1
;	RETI
