.include "m16def.inc"	
.def TMP1=R16		;
.def TMP2=R17		;
.def TMP3=R18

.cseg			
.org $000		;locate code at address $000
rjmp START		; Jump to the START Label
.org INT0addr
rjmp INT0_ISR
.org INT1addr
rjmp INT1_ISR
.org $02A		;locate code past the interupt vectors

START: 	
	LDI TMP1, LOW(RAMEND)	;initialise the stack pointer
	OUT SPL, TMP1
	LDI TMP1, HIGH(RAMEND)
	OUT SPH, TMP1

	RCALL INITIALISE_PORTS
	RCALL INITIALISE_INTERRUPTS	
	SEI

MAIN_LOOP:
	NOP
	NOP
	NOP
	RJMP MAIN_LOOP

DELAY:
		PUSH TMP3
		PUSH TMP2
		PUSH TMP1
		SER TMP1		; TMP1=0xff
Del1:	SER TMP2		; TMP2=0xff
Del2:	LDI TMP3, 10	; TMP=10 decimal
Del3:	DEC TMP3		; decrement TMP3
		BRNE Del3
		DEC TMP2
		BRNE Del2
		DEC TMP1
		BRNE Del1
		POP TMP1
		POP TMP2
		POP TMP3
		RET

INITIALISE_PORTS:
	SER TMP1
	OUT DDRA, TMP1		; set DDRA in output mode
	CLR TMP1			; clear variable
	OUT PORTA, TMP1		; all LED's off
	OUT DDRD, TMP1		; set DDRD in input mode
	SBI PORTD, 2		; enable pull-ups on PD2
	SBI PORTD, 3		; enable pull-ups on PD3
	RET

INITIALISE_INTERRUPTS:
	LDI TMP1, 0x40		; initialise interrupts for INT0
	OUT GICR, TMP1
	LDI TMP1, 0x0a		; falling edge INT0, falling edge INT1
	OUT MCUCR, TMP1
	RET

INT1_ISR:	
	CLR TMP1
	OUT TCCR0, TMP1		; stop timer
	IN TMP1, TCNT0		; read in timer value
	OUT PORTA, TMP1		; display random counter value on LED's
	RCALL Delay
	LDI TMP1, 0x40		; initialise interrupts for INT0
	OUT GICR, 0x0a

	RETI

INT0_ISR:	
	LDI TMP1, 0x80		; disable INT0, enable INT1
	RCALL Delay
	OUT GICR, TMP1
	CLR TMP1
	OUT TCNT0, TMP1		; set timer value to zero
	OUT GIFR, TMP1		; clear flags
	LDI TMP1, 0x01
	OUT TCCR0, TMP1		; set up timer with no pre-scaling		
	RETI
	
	
