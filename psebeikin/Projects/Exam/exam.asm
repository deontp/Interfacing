.include "m16def.inc"	

.def TMP1=R16		
.def TMP2=R17		
.def rxpos=r19
.def ZERO=R20
.def MASK=R21
.def COUNT=R22
.dseg 
RXBUFFER: .byte 10 ; reserve 10bytes for the message received
LCDbuffer: .byte 10 ; lcd time buffer

.cseg			
.org $000		;locate code at address $000
jmp START		; Jump to the START Label
.org URXCaddr
jmp URXC_ISR	
.org UTXCaddr
jmp UTXC_ISR
 
.org $02A		;locate code past the interupt vectors

START: 	
	LDI TMP1, LOW(RAMEND)	                     ;initialise the stack pointer
	OUT SPL, TMP1
	LDI TMP1, HIGH(RAMEND)
	OUT SPH, TMP1	
	CALL INIT_UART
  	CALL SETUP_PORTS
	CALL SEND_MENU
	CALL SETUP_MOTOR
	LDI TMP1, 0x00
	MOV ZERO, TMP1
	SEI

MAIN_LOOP:
	NOP
	NOP
	RJMP MAIN_LOOP

SETUP_PORTS:
	LDI TMP1, 0xff	
	OUT DDRA, TMP1								; set all bits in PORTA to output mode.
	OUT PORTA, ZERO								; all LED's initially off		
	RET

SETUP_MOTOR:	
	LDI TMP1, 0xf0
	IN TMP2, DDRD
	OR TMP2, TMP1
	OUT DDRD, TMP2								; setting PD
	LDI MASK, 0x80
    OUT PORTD, MASK
	RET

SEND_MENU:
    ; Z is reserved for pointing to next char
	; Z register is used specifically for program memory because LPM instruction
	; only works with Z register.
    LDI ZL, LOW(2*TEXT_MENU)
    ldi ZH, HIGH(2*TEXT_MENU)
	LPM TMP1, Z+
	OUT UDR, TMP1
    RET

INIT_UART:
;set baud rate (9600,8,n,2)
 	LDI TMP1, 51				; page 173 in ATMega Datasheet
	LDI TMP2, 0x00
	OUT UBRRH, TMP2				; set to 0 beause we don't use the upper 8 bits 
	OUT	UBRRL, TMP1				; set to 51 for the USART config
;set rx and tx enable
	SBI UCSRB, RXEN
	SBI UCSRB, TXEN
; enable uart interrupts
	SBI UCSRB, RXCIE
    SBI UCSRB, TXCIE
	RET

; Interrupt code for when UDR empty
; Code for TX complete 
UTXC_ISR:
	 LPM TMP1, Z+
	 CPI TMP1, 0x00
	 BREQ EXIT_TX_ISR
	 OUT UDR, TMP1
	 RETI

EXIT_TX_ISR:
;diable txcomplete int
  IN TMP1, UCSRB						; page 168 in the ATMega16 Data Sheet
  ANDI TMP1, 0b10111111					; no more characters will be sent via USART
  OUT UCSRB, TMP1  
  RETI

; Code for RX complete
URXC_ISR:
    IN TMP1, UDR
    LDI XL,LOW(RXBUFFER)				; acquires the position in RAM where message to be stored
    LDI XH,HIGH(RXBUFFER)
	ADD XL, RXPOS
	ADC XH, ZERO
    ST X, TMP1
    CPI TMP1, '.'
	BREQ PARSE_MESSAGE
	INC RXPOS
	RETI

PARSE_MESSAGE:
    LDI XL, LOW(RXBUFFER)
    LDI XH, HIGH(RXBUFFER)
	LD TMP1, X
	CPI TMP1, '1'
	BREQ TASK_1
	CPI TMP1, '2'
	BREQ TASK_2
	CPI TMP1, '3'
	BREQ TASK_3	
	CPI TMP1, '4'
	BREQ TASK_4
	CPI TMP1, '5'
	BREQ TASK_5
	CPI TMP1, '6'
	BREQ TASK_6
	CPI TMP1, '7'
	BREQ TASK_7
	CPI TMP1, '8'
	BREQ TASK_8
	CPI TMP1, '9'
	BREQ TASK_9
EXIT_PARSE:
    MOV RXPOS, ZERO
	RETI

TASK_1:											; single step anti-clockwise			
   MOV TMP1, MASK								; skip task if stepper motor is disabled
   CPI TMP1, 0xf0								
   BREQ EXIT_PARSE
   LDI TMP1, 0xff
   OUT PORTA, TMP1
   LSL MASK
   CPI MASK, 0x00
   BRNE OUTPUT_1 
   LDI MASK, 0b00010000
OUTPUT_1:
   OUT PORTD, MASK
   RJMP EXIT_PARSE
TASK_2:											; single step clockwise
   MOV TMP1, MASK								; skip task if stepper motor is disabled
   CPI TMP1, 0xf0								
   BREQ EXIT_PARSE
   OUT PORTA, ZERO
   LSR MASK
   CPI MASK, 0b00001000
   BRNE OUTPUT_2
   LDI MASK, 0b10000000
OUTPUT_2:
   OUT PORTD, MASK
   RJMP EXIT_PARSE
TASK_3:
   LDI COUNT, 0x00
   CPI COUNT, 50
   BREQ EXIT_PARSE
   MOV TMP1, MASK								; skip task if stepper motor is disabled
   CPI TMP1, 0xf0								
   BREQ EXIT_PARSE
   LDI TMP1, 0xff
   OUT PORTA, TMP1
   LSL MASK
   CPI MASK, 0x00
   BRNE OUTPUT_3 
   LDI MASK, 0b00010000
OUTPUT_3:
   OUT PORTD, MASK
   INC COUNT
   RJMP TASK_3
TASK_4:
TASK_5:											; disable stepper motor
	IN TMP1, DDRD
	ANDI TMP1, 0x0f
	OUT DDRD, TMP1
	IN TMP1, PORTD
	ANDI TMP1, 0x0f
	OUT PORTD, TMP1
	LDI TMP1, 0xf0
	MOV MASK, TMP1								; set mask to mimic stepper motor "offline" mode
	RJMP EXIT_PARSE		
TASK_6:											; enable stepper motor
	CALL SETUP_MOTOR
	RJMP EXIT_PARSE
TASK_7:
TASK_8:
TASK_9:
TASK_10:
TASK_11:

; stored in program memory (CSEG)
TEXT_MENU: 
.db "PROJECT TASKS: ",0x0d,0x0a, \
  "---------------", 0x0d, 0x0a, \
  "1) Single step clockwise", 0x0d,0x0a, \
  "2) Single step anti-clockwise", 0x0d,0x0a, \
  "3) 180 clockwise", 0x0d,0x0a, \
  "4) 180 anti-clockwise", 0x0d,0x0a, \
  "5) Disable Stepper", 0x0d,0x0a, \
  "6) Enable Stepper", 0x0d,0x0a, \
  "7) Start ADC PWM Task", 0x0d,0x0a, \
  "8) Stop ADC PWM Task", 0x0d,0x0a, \
  "9) Print 3rd stored message to LCD", 0x0d,0x0a, \
  "10) Clear LCD",0x0d,0x0a, \
  "11) Reset Micro-controller",0x0d,0x0a,0x00,0x00

.include "LCD.asm"
