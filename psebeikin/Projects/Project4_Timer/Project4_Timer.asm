; reaction timer
; switch0 - int0 on pd2
; switch1 - int1 on pd3
; portb0-7 to leds 0-7

.include "m16def.inc"	

.def TMP1=R16		;
.def TMP2=R17		;
.def TMP3=R18
.def COUNT=R19
.def SECS=R20

.cseg			
.org $000		;locate code at address $000
rjmp START		; Jump to the START Label
.org INT0addr
rjmp INT0_ISR
.org INT1addr
rjmp INT1_ISR
.org OVF1addr
rjmp TIMER1_OVF_ISR
.org OC1Baddr
rjmp TIMER1_OCR_ISR
.org $02A		;locate code past the interupt vectors

START: 	
	ldi TMP1, LOW(RAMEND)	;initialise the stack pointer
	out SPL, TMP1
	ldi TMP1, HIGH(RAMEND)
	out SPH, TMP1

	RCALL INITIALISE_PORTS
	RCALL INITIALISE_TIMER
	RCALL INITIALISE_EXTERNAL_INTERRUPTS
	SEI
MAIN_LOOP:
	NOP
	NOP
	RJMP MAIN_LOOP

INITIALISE_PORTS:
	; portb output LEDs
	LDI TMP1, 0x00		; clear all bits in TMP1
	out PORTA, tmp1		; LED's all off	
	LDI TMP1, 0xFF		; set all bits in TMP1
	OUT DDRA, TMP1		; set output mode
	;portd inputs
	LDI TMP1, 0x00		; clear all bits in TMP1
	OUT DDRD, TMP1		; set input mode
	;enable pullups on int pins
	SBI PORTD, 2		
	SBI PORTD, 3
	RET

INITIALISE_TIMER:
	;enable timer 1 interrupt
	CLR TMP1
	LDI TMP1, 0x04
	OUT TIMSK, tmp1
	clr TMP1
	;set initial value in timer 
	OUT TCNT1H, TMP1	; setting upper 4 bits to zero
	OUT TCNT1L, TMP1	; setting lower 4 bits to zero	

	RET

INITIALISE_EXTERNAL_INTERRUPTS:
	;enable  int0
	LDI TMP1, 0x40
	OUT GICR, TMP1
	; interrupt on falling edge.
	LDI TMP1, 0x0a
	OUT MCUCR, TMP1
	RET

INT0_ISR:
	;wait a while and then light the test led
;disable int 0 and enable int1
	LDI TMP1, 0x80 		; disables int0 and enables int1
	OUT GICR, TMP1
	SBI PORTA,0			; light the LED on Port A bit 0
    LDI TMP1, 0x60
; DELAY
loop1: SER TMP2
loop2: SER TMP3
loop3: ;
	DEC TMP3
	CPI TMP3, 0
	BRNE loop3
	DEC TMP2
	BRNE loop2
	DEC TMP1
	BRNE loop1
	;we have now finished a bit of a delay.
	SBI PORTA, 1 ;turn led on
	CBI PORTA,0	; turn led off
	;start timer going
	LDI TMP1, 0x05
	OUT TCCR1B, TMP1
	RETI

INT1_ISR:
; stop timer1
	LDI TMP1, 0x00
	OUT TCCR1B, TMP1
;disable int 1 and enable int0
	LDI TMP1, 0x40
	OUT GICR, TMP1
;read both timer bytes and display (we will only display the high byte}
    ;IN TMP2, TCNT1L
	;IN TMP1, TCNT1H
	;out PORTA,TMP1
	RETI

TIMER1_OVF_ISR:
; too slow
	LDI TMP1, 0x00
	OUT TCCR1B, TMP1 ; stops the clock
	OUT PORTA, TMP1
	RETI

TIMER1_OCR_ISR:
	INC COUNT
	CPI COUNT, 10
	BRNE TIMER1_OCR_ISR_RET	
	INC SECS
TIMER1_OCR_ISR_RET:
	OUT PORTA, SECS
	RETI
	
	
