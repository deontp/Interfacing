.include "m16def.inc"	
.def TMP1=R16		;
.def TMP2=R17		;
.def TMP3=R18
.def COUNT=R19
.def SECS=R20

.cseg			
.org $000		;locate code at address $000
rjmp START		; Jump to the START Label
.org INT0addr
rjmp INT0_ISR
.org OVF1addr
rjmp TIMER1_OVF_ISR
.org OC1Baddr
rjmp TIMER1_OCR_ISR
.org $02A		;locate code past the interupt vectors

START: 	
	ldi TMP1, LOW(RAMEND)	;initialise the stack pointer
	out SPL, TMP1
	ldi TMP1, HIGH(RAMEND)
	out SPH, TMP1

	LDI COUNT, 0x00			; clear counter	
	LDI SECS, 0x00
	RCALL INITIALISE_PORTS
	RCALL INITIALISE_TIMER
	RCALL INITIALISE_EXTERNAL_INTERRUPTS
	SEI
MAIN_LOOP:
	NOP
	NOP
	RJMP MAIN_LOOP

DELAY:
	push tmp3
	push tmp2
	push tmp1

		ser TMP1		; TMP1=0xff
Del1:	ser TMP2		; TMP2=0xff
Del2:	ldi TMP3, 10	; TMP=10 decimal
Del3:	dec TMP3		; decrement TMP3
		brne Del3
		dec TMP2
		brne Del2
		dec TMP1
		brne Del1
	pop tmp1
	pop tmp2
	pop tmp3
		ret

INITIALISE_PORTS:
	; portb output LEDs
	LDI TMP1, 0x00		; clear all bits in TMP1
	out PORTA, tmp1		; LED's all off	
	LDI TMP1, 0xFF		; set all bits in TMP1
	OUT DDRA, TMP1		; set output mode
	;portd inputs
	LDI TMP1, 0x00		; clear all bits in TMP1
	OUT DDRD, TMP1		; set input mode
	;enable pullups on int pins
	SBI PORTD, 2		
	SBI PORTD, 3
	RET

INITIALISE_TIMER:
	;enable timer 1 interrupt
	;LDI TMP1, 0b00000101
	;OUT TCCR1B, TMP1
	CLR TMP1
	LDI TMP1, 0b00001000
	OUT TIMSK, tmp1
	clr TMP1
	;set initial value in timer 
	OUT TCNT1H, TMP1	; setting upper 4 bits to zero
	OUT TCNT1L, TMP1	; setting lower 4 bits to zero

	LDI TMP1, 0x00
	OUT OCR1BH, TMP1 
	LDI TMP1, 0x1E
	OUT OCR1BL, TMP1	
	RET

INITIALISE_EXTERNAL_INTERRUPTS:
	;enable  int0
	LDI TMP1, 0x40
	OUT GICR, TMP1
	; interrupt on falling edge.
	LDI TMP1, 0x0a
	OUT MCUCR, TMP1
	RET

INT0_ISR:	
	;start timer going
	LDI TMP1, 0b00000101
	OUT TCCR1B, TMP1
	RETI

TIMER1_OCR_ISR:
	INC COUNT	
	
	CLR TMP1
	OUT TCNT1H, TMP1	; setting upper 4 bits to zero
	OUT TCNT1L, TMP1	; setting lower 4 bits to zero
	;OUT PORTA, COUNT
	;RETI
	CPI COUNT, 10
	BRNE TIMER1_OCR_ISR_RET	
	INC SECS
TIMER1_OCR_ISR_RET:
	OUT PORTA, SECS
	;RCALL DELAY
	RETI

TIMER1_OVF_ISR:
; too slow
	LDI TMP1, 0x00
	OUT TCCR1B, TMP1 ; stops the clock
	OUT PORTA, TMP1
	RETI
