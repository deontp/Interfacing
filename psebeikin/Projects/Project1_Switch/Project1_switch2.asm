;LED-SWITCH - Project1
; This program toggles portB pin 0
; Remember to patch PortB0 to an LED
; and PORTD0 to Switch0
.include "m16def.inc"	;Can you think why we need this

.def TMP1=R16		;defines serve the same purpose as in C,	
.def TMP2=R17		;before assembly, defined values are substituted
.def TMP3=R18

.cseg			;Tell the assembler that everything below this is in the code segment

.org $000		;locate code at address $000
rjmp START		; Jump to the START Label

.org $02A		;locate code past the interupt vectors

START: 	ldi TMP1, LOW(RAMEND)	;initialise the stack pointer
	out SPL, TMP1
	ldi TMP1, HIGH(RAMEND)
	out SPH, TMP1

	RCALL INITIALISE ; Call the subroutine INITIALISE

MAIN_LOOP: 
	; Due to instructions we deviate slightly from 
	;	our flow diagram
	; code for switch 1
	SBIC PIND, 0	;skip next instruction if d:0 is low
	CBI  PORTB, 0	;if d:0 is high (nopress) make b:0 low (off)
	SBIS PIND, 0    ;skip if d:0 set (no press)
	SBI	 PORTB, 0	; if pressed take b:0 high (led on)
	
	; code for switch 2
	SBIC PIND, 1	;skip next instruction if d:1 is low
	CBI  PORTB, 1	;if d:1 is high (nopress) make b:1 low (off)
	SBIS PIND, 1    ;skip if d:1 set (no press)
	SBI	 PORTB, 1	; if pressed take b:1 high (led on)

	; code for switch 3
	SBIS PIND, 1	; skip if switch 1 not pressed
	RCALL SWITCH3	; if switch 1 is pressed, call Switch3 to turn on LED

	NOP
	NOP
	RJMP MAIN_LOOP

SWITCH3:
	SBIC PIND, 2	;skip next instruction if d:3 is low
	CBI  PORTB, 2	;if d:3 is high (nopress) make b:3 low (off)
	SBIS PIND, 2    ;skip if d:3 set (no press)
	SBI	 PORTB, 2	; if pressed take b:3 high (led on)
	RET



INITIALISE:
	; code for switch 1
	SBI DDRB, 0			; Make pin0 of portB output
	CBI PORTB, 0		; Set the initial state of pin to low=LED-off
	
	CBI DDRD, 0 	;make D:0 an input
	SBI PORTD, 0	; enable pull-up on D:0

	; code for switch 2
	SBI DDRB, 1			; Make pin1 of portB output
	CBI PORTB, 1		; Set the initial state of pin to low=LED-off
	
	CBI DDRD, 1 	;make D:1 an input
	SBI PORTD, 1	; enable pull-up on D:1

	; code for switch 3
	SBI DDRB, 2			; Make pin2 of portB output
	CBI PORTB, 2		; Set the initial state of pin to low=LED-off
	
	CBI DDRD, 2 	;make D:2 an input
	SBI PORTD, 2	; enable pull-up on D:2
		
	RET					;Return from subroutine
