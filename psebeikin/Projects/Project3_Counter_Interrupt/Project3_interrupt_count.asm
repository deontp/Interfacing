;button_count
; This program counts button presses on 
; portb0 and displays them on portd leds
; using interrupts

.include "m16def.inc"	;Can you think why we need this

.def TMP1=R16		;
.def TMP2=R17		;
.def TMP3=R18
.def COUNT=R19	;store the count in this register

.cseg			;Tell the assembler that everything below this is in the code segment

.org 0x000		;locate code at address $000
rjmp START		; Jump to the START Label
.org INT0addr	; initialising interrupt vectors
rjmp INT0_ROUTINE ; will get executed when the interrupt occurs
.org INT1addr
rjmp INT1_ROUTINE

.org $02A		;locate code past the interupt vectors

START: 	
	ldi TMP1, LOW(RAMEND)	;initialise the stack pointer
	out SPL, TMP1
	ldi TMP1, HIGH(RAMEND)
	out SPH, TMP1

	RCALL INITIALISE
	SEI
MAIN_LOOP:
	SBIS PIND, 4
	RCALL DISABLE_DEC
	NOP
	NOP
	NOP
	RJMP MAIN_LOOP

DISABLE_DEC:
	PUSH TMP1
	
	RCALL Delay	
	IN TMP1, GICR
	LDI TMP3, 0b10000000	
	EOR TMP1, TMP3
	OUT GICR, TMP1

	IN TMP1, GIFR
	ANDI TMP1, 0b01000000	
	OUT GIFR, TMP1
		
	POP TMP1
	RET

DELAY:
	push tmp3
	push tmp2
	push tmp1

		ser TMP1		; TMP1=0xff
Del1:	ser TMP2		; TMP2=0xff
Del2:	ldi TMP3, 10	; TMP=10 decimal
Del3:	dec TMP3		; decrement TMP3
		brne Del3
		dec TMP2
		brne Del2
		dec TMP1
		brne Del1
	pop tmp1
	pop tmp2
	pop tmp3
		ret

INITIALISE:
	; Setup port a as output for LEDs 
	; and inital state to all off and set counter to zero
	CLR TMP1;
	OUT DDRD, TMP1	; ensure that portdis an input
	;SBI PORTD,2		; enable pull-ups on int0
	;SBI PORTD,3		; enable pull-ups on int1
	;SBI PORTD, 4	; enable pull-ups portd4
	;CLR TMP1
	OUT PORTA, TMP1	; port A all low, so leds off
    SER TMP1
	OUT PORTD, TMP1	; enable pull-ups on ports D
	OUT DDRA, TMP1	; porta output
	CLR COUNT		; clear the counter
	LDI TMP1, 0b00001111	; int0 falling, int1 rising
	OUT	MCUCR, TMP1
	LDI TMP1, 0xc0  ; 0b11000000
	OUT GICR, TMP1  ; enabling interrupt bits for int0 and int1 in global interrupt register
	RET

INT0_ROUTINE:
;int 0 is to increment the counter	
	PUSH TMP1		; save tmp1
	RCALL Delay

	IN TMP1, GIFR
	ANDI TMP1, 0b01000000	
	OUT GIFR, TMP1
	  
	IN TMP1, SREG	; transfer status register into TMP1
	PUSH TMP1		; save sreg
	INC COUNT		; increment the counter
	MOV TMP1, COUNT ; store the counter in TMP1
	OUT PORTA, TMP1	; output
	POP TMP1		; pop the status register off of the stack and store in TMP1
	OUT SREG, TMP1	; restore sreg	
	
	POP TMP1		; restore tmp1
	RETI

INT1_ROUTINE:
;int 1 is to decrement the counter
	RCALL Delay
	PUSH TMP1	

	IN TMP1, GIFR
	ANDI TMP1, 0b10000000	
	OUT GIFR, TMP1

	IN TMP1, SREG
	PUSH TMP1
	DEC COUNT		; decrement the counter
	MOV TMP1, COUNT
;;	COM TMP1
	OUT PORTA, TMP1
	POP TMP1
	OUT SREG, TMP1	

	POP TMP1
	RETI


