; Button counter: increment and decrement using sw0 and sw1
; AUTHOR: DYLAN SMITH 
; Used PORTA instead of PORTB to connecte to the LEDs due to apparent easter egg.
; PORTA 0-7 --> LED 0-7
; PORTD 0-1 --> SW 0-1
.include "m16def.inc"	;Can you think why we need this

.def TMP1=R16		;
.def TMP2=R17		;
.def TMP3=R18
.def COUNT=R19		;store the count in this register (can still use this to decrement when sw1 is pressed)

.cseg			;Tell the assembler that everything below this is in the code segment

.org $000		;locate code at address $000
rjmp START		; Jump to the START Label

.org $02A		;locate code past the interupt vectors

START: 	
	ldi TMP1, LOW(RAMEND)	;initialise the stack pointer
	out SPL, TMP1
	ldi TMP1, HIGH(RAMEND)
	out SPH, TMP1

	RCALL INITIALISE

CHECK_BTN_PRESS:
	SBIS PIND, 0											; bit is set, so sw1 is released - go to BTN_INC_D
	RCALL BTN_INC_D
	SBIS PIND, 1											; bit is set, so sw0 is released - go to BTN_DEC_D
	RCALL BTN_DEC_D
	RJMP CHECK_BTN_PRESS									; repeat checking process to see that previous button has been released 

BTN_INC_D:											  	    ; handles the pushing of increment button (i.e. sw0) 
	INC COUNT												; decrement the counter
	MOV TMP1, COUNT  										; move value into a temporary register
	OUT PORTA, TMP1                                         ; Output the value to the LED	

BTN_INC_U:													; Handle when button isnt pushed						
	SBIC PIND, 0 											; skip if button released
	RET 													
	RJMP BTN_INC_U


BTN_DEC_D:													; Handles the pushing of the decrement button (i.e. sw1)
	DEC COUNT												; decrement the counter
	MOV TMP1, COUNT 										; move decremented value into TMP1
	OUT PORTA, TMP1 										; output decremented value

BTN_DEC_U:													; Handle when button isnt pushed						
	SBIC PIND, 1 											; skip if button released
	RET
	RJMP BTN_DEC_U											; button not test released

INITIALISE: 												; Setup port b as output for LEDs 
															; and inital state to all off and set counter to zero
	SER TMP1;
	OUT DDRA, TMP1
    CLR TMP1
	OUT PORTA, TMP1 										; 0x00 = all off
	CLR COUNT
	OUT DDRD, TMP1 											; make d inputs (tmp1 is a handy clear register)
    SER TMP1
	OUT PORTD, TMP1 										; activate pullups on PORTD
	RET